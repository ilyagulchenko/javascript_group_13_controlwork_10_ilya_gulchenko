const express = require('express');
const db = require('../secFileDb');

const router = express.Router();

router.get('/', (req,res) => {
    const comments = db.getComments();

    return res.send(comments);
});

module.exports = router;
