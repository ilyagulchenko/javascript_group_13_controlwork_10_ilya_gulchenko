import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewsService } from '../../../services/news.service';
import { NewsData } from '../../../models/news.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.sass']
})
export class AddNewsComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(
    private newsService: NewsService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  createNews() {
    const newsData: NewsData = this.form.value;
    this.newsService.createNews(newsData).subscribe(() => {
      void this.router.navigate(['/']);
    });
  }

}
