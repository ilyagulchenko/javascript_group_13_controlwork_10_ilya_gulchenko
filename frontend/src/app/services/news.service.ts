import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News, NewsData } from '../models/news.model';
import { environment } from '../../environments/environment';
import { map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  newsFetch = new Subject<News[]>();

  constructor(private http: HttpClient) {}

  private news: News[] = [];

  getNews() {
    return this.http.get<News[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(newsData => {
          return new News(
            newsData.id,
            newsData.title,
            newsData.content,
            newsData.image,
            new Date(newsData.date).toLocaleString(),
          );
        });
      })
    ).subscribe(news => {
      this.news = news;
      this.newsFetch.next(this.news);
    })
  }

  getNewsById(id: string) {
    return this.http.get<News>(environment.apiUrl + `/news/${id}`).pipe(
      map(result => {
        return new News(
          result.id,
          result.title,
          result.content,
          result.image,
          new Date(result.date).toLocaleString(),
        );
      })
    )
  }

  createNews(newsData: NewsData) {
    const formData = new FormData();

    Object.keys(newsData).forEach(key => {
      if (newsData[key] !== null) {
        formData.append(key, newsData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/news', formData);
  }

  removeNews(id: string) {
    return this.http.delete(environment.apiUrl + `/news/${id}`);
  }

}
