import { Component, OnInit } from '@angular/core';
import { News } from '../../models/news.model';
import { NewsService } from '../../services/news.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit {
  newsChangeSubscription!: Subscription;

  allNews: News[] = [];

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.newsChangeSubscription = this.newsService.newsFetch.subscribe((allNews: News[]) => {
      this.allNews = allNews;
    });
    this.newsService.getNews();
  }

  onRemove(id: string) {
    this.newsService.removeNews(id).subscribe(() => {
      this.newsService.getNews();
    })
  }

}
