import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { News } from '../../models/news.model';
import { NewsService } from '../../services/news.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewsResolverService implements Resolve<News> {

  constructor(private newsService: NewsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<News> {
    const newsId = <string>route.params['id'];
    return this.newsService.getNewsById(newsId);
  }
}
