const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const fileName = './db.json';
let data = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(fileName);
            data = JSON.parse(fileContents.toString());
        } catch (e) {
            data = [];
        }
    },
    getAllNews() {
        return data;
    },
    getNews(id) {
        return data.find(p => p.id === id);
    },
    addNews(news) {
        news.id = nanoid();
        news.date = new Date().toISOString();
        data.push(news);
        return this.save();
    },
    getUserIndex(id) {
      const index = data.findIndex(u => u.id === id);
      data.splice(index, 1);
      return data;
    },
    save() {
        return fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
}
