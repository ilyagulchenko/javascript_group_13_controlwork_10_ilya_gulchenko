const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req,res) => {
    const news = db.getAllNews();

    return res.send(news);
});

router.get('/:id', (req,res) => {
    const news = db.getNews(req.params.id);

    if (!news) {
        return res.status(404).send({message: 'Not found'});
    }

    return res.send(news);
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        const news = {
            title: req.body.title,
            content: req.body.content,
        };

        if (req.file) {
            news.image = req.file.filename;
        }

        await db.addNews(news);

        return res.send({message: 'Created news!'});
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', (req, res) => {
    const news = db.getUserIndex(req.params.id);

    return res.send(news);
});

module.exports = router;
