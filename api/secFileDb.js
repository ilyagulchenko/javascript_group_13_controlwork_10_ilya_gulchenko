const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const fileName = './secDb.json';
let secData = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(fileName);
            secData = JSON.parse(fileContents.toString());
        } catch (e) {
            secData = [];
        }
    },
    getComments() {
        return secData;
    },
    addComment(comment) {
        comment.id = nanoid();
        secData.push(comment);
        return this.save();
    },
    save() {
        return fs.writeFile(fileName, JSON.stringify(secData, null, 2));
    }
}
