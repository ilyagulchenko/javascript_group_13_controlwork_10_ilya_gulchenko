import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NewsComponent} from './pages/news/news.component';
import {AddNewsComponent} from './pages/news/add-news/add-news.component';
import {NewsItemComponent} from './pages/news/news-item/news-item.component';
import {NewsResolverService} from './pages/news/news-resolver.service';

const routes: Routes = [
  {path: '', component: NewsComponent},
  {path: 'news/new', component: AddNewsComponent},
  {path: 'news/:id', component: NewsItemComponent, resolve: {
    news: NewsResolverService
    }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
